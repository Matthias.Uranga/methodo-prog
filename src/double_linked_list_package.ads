with Ada.Strings.Unbounded, Ada.Text_IO, Ada.Text_IO
  .Unbounded_IO, metadata_package;
use Ada.Strings.Unbounded, Ada.Text_IO, Ada.Text_IO.Unbounded_IO,
  metadata_package;

generic
   type T_generic_content is private;
   with function "="
     (l_element : in T_generic_content; r_element : in T_generic_content)
      return Boolean;
   with function ">"
     (l_element : in T_generic_content; r_element : in T_generic_content)
      return Boolean;
   with function "<"
     (l_element : in T_generic_content; r_element : in T_generic_content)
      return Boolean;

package double_linked_list_package is
   LIST_EMPTY        : exception;
   NO_ELEMENT_BEFORE : exception;
   NO_ELEMENT_AFTER  : exception;

   type T_double_link_list is private;

-----------------------------------------------------------------------------
--                 MOVEMENTS AND "CHECKERS" ON NEXT & PREVIOUS             --
-----------------------------------------------------------------------------
   -- Name: got_Next
   -- Semantics: return wether the list's current element has a next element or not
   -- Parameters:
   --      F_dbl_list: double linked list on which you want to know if the current element has a next element
   -- Return type: Boolean
   -- Preconditions: /
   -- Postconditions: /
   function got_Next (F_dbl_list : in T_double_link_list) return Boolean;

   -- Name: got_Previous
   -- Semantics: return wether the list's current element has a previous element or not
   -- Parameters:
   --      F_dbl_list: double linked list on which you want to know if the current element has a previous element
   -- Return type: Boolean
   -- Preconditions: /
   -- Postconditions: /
   function got_Previous (F_dbl_list : in T_double_link_list) return Boolean;
-----------------------------------------------------------------------------

   -- Name: goTo_Next
   -- Semantics: sets list's current element as its next element
   -- Parameters:
   --      F_dbl_list: the list you want to change the current element to
   -- Exceptions: NO_ELEMENT_AFTER,
   -- Preconditions: /
   -- Postconditions: /
   procedure goTo_Next (F_dbl_list : in out T_double_link_list);

   -- Name: goTo_Previous
   -- Semantics: sets list's current element as its previous element
   -- Parameters:
   --      F_dbl_list: the list you want to change the current element to
   -- Exceptions: NO_ELEMENT_BEFORE,
   -- Preconditions: /
   -- Postconditions: /
   procedure goTo_Previous (F_dbl_list : in out T_double_link_list);
-----------------------------------------------------------------------------

   -- Name: go_To
   -- Semantics: sets list's current element to any element you want
   -- Parameters:
   --      F_dbl_list: the list you want to change the current element to
   --      F_element_toGoTo_content: the element you want to sets list's current element to
   -- Preconditions: /
   -- Postconditions: /
   procedure go_To
     (F_dbl_list               : in out T_double_link_list;
      F_element_toGoTo_content :        T_generic_content);
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--                            GETTERS ON ALL ELEMENTS                      --
-----------------------------------------------------------------------------

   -- Name: get_Next_Content
   -- Semantics: returns the current element's next value
   -- Parameters:
   --      F_dbl_list: the list you want to get the element next to its current
   -- Return type: T_generic_content
   -- Preconditions: /
   -- Postconditions: /
   function get_Next_Content
     (F_dbl_list : in T_double_link_list) return T_generic_content;

   -- Name: get_Previous_Content
   -- Semantics: returns the current element's previous value
   -- Parameters:
   --      F_dbl_list: the list you want to get the element previous to its current
   -- Return type: T_generic_content
   -- Preconditions: /
   -- Postconditions: /
   function get_Previous_Content
     (F_dbl_list : in T_double_link_list) return T_generic_content;

   -- Name: get_First_Content
   -- Semantics: returns the list's first element
   -- Parameters:
   --      F_dbl_list: the list you want to get the first element of
   -- Return type: T_generic_content
   -- Preconditions: /
   -- Postconditions: /
   function get_First_Content
     (F_dbl_list : in T_double_link_list) return T_generic_content;

   -- Name: get_Last_Content
   -- Semantics: returns the list's last element
   -- Parameters:
   --      F_dbl_list: the list you want to get the last element of
   -- Return type: T_generic_content
   -- Preconditions: /
   -- Postconditions: /
   function get_Last_Content
     (F_dbl_list : in T_double_link_list) return T_generic_content;

   -- Name: get_Content
   -- Semantics: returns the list's current element
   -- Parameters:
   --      F_dbl_list: the list you want to get the current element of
   -- Return type: T_generic_content
   -- Preconditions: /
   -- Postconditions: /
   function get_Content
     (F_dbl_list : in T_double_link_list) return T_generic_content;
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--                         CREATER, ADDERS AND REMOVER                     --
-----------------------------------------------------------------------------

   -- Name: createList
   -- Semantics: creates a new empty list
   -- Parameters:
   -- Return type: T_double_link_list
   -- Preconditions: /
   -- Postconditions: /
   function createList return T_double_link_list;

   -- Name: addLast
   -- Semantics: adds an element to the list, as its last
   -- Parameters:
   --      F_dbl_list: the list you want to add the element to
   --      F_newElement_content: the value of the new element
   -- Preconditions: /
   -- Postconditions: /
   procedure addLast
     (F_dbl_list           : in out T_double_link_list;
      F_newElement_content : in     T_generic_content);

   -- Name: addFirst
   -- Semantics: adds an element to the list, as its first
   -- Parameters:
   --      F_dbl_list: the list you want to add the element to
   --      F_newElement_content: the value of the new element
   -- Preconditions: /
   -- Postconditions: /
   procedure addFirst
     (F_dbl_list           : in out T_double_link_list;
      F_newElement_content : in     T_generic_content);

   -- Name: addAfter
-- Semantics: adds an element to the list, right after the current list element
-- Parameters:
   --      F_dbl_list: the list you want to add the element to
   --      F_newElement_content: the value of the new element
   -- Preconditions: /
   -- Postconditions: /
   procedure addAfter
     (F_dbl_list           : in out T_double_link_list;
      F_newElement_content : in     T_generic_content);

   -- Name: addBefore
   -- Semantics: adds an element to the list, right before the current list element
   -- Parameters:
   --      F_dbl_list: the list you want to add the element to
   --      F_newElement_content: the value of the new element
   -- Preconditions: /
   -- Postconditions: /
   procedure addBefore
     (F_dbl_list           : in out T_double_link_list;
      F_newElement_content : in     T_generic_content);

   -- Name: removeElement
   -- Semantics: removes an element off the list
   -- Parameters:
   --      F_dbl_list: the list you want to add the element to
   --      F_elementToRemove_content: the value of the element to remove
   -- Preconditions: /
   -- Postconditions: /
   procedure removeElement
     (F_dbl_list                : in out T_double_link_list;
      F_elementToRemove_content : in     T_generic_content);
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--                      UTILS : LENGTH, EMPTY CHECKER                      --
-----------------------------------------------------------------------------

   -- Name: get_length
   -- Semantics: returns the list's length
   -- Parameters:
   --      F_dbl_link_list: the list you want to know the length
   -- Return type: Natural
   -- Preconditions: /
   -- Postconditions: /
   function get_Length (F_dbl_list : in T_double_link_list) return Natural;

   -- Name: isEmpty
   -- Semantics: returns wether the list is empty or not
   -- Parameters:
   --      F_dbl_link_list: the list you want to know its emptiness
   -- Return type: Boolean
   -- Preconditions: /
   -- Postconditions: /
   function isEmpty (F_dbl_list : in T_double_link_list) return Boolean;

   -- Name: empty
   -- Semantics: empties the list
   -- Parameters:
   --      F_dbl_link_list: the list you want to empty
   -- Return type: Natural
   -- Preconditions: /
   -- Postconditions: /
   procedure empty (F_dbl_list : in out T_double_link_list);
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

private
   type T_list_element;
   type T_list_element_PTR is access T_list_element;
   type T_list_element is record
      next     : T_list_element_PTR;
      previous : T_list_element_PTR;
      content  : T_generic_content;
   end record;

   type T_double_link_list is record
      first                : T_list_element_PTR;
      last                 : T_list_element_PTR;
      current_list_element : T_list_element_PTR;
      length               : Natural;
   end record;

   procedure addToEmptyList(F_dbl_list : in out T_double_link_list; F_newElement_content : in T_generic_content);

end double_linked_list_package;
