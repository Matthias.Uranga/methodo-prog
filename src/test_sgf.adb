with Ada.strings.unbounded, Ada.Text_IO.Unbounded_IO, metadata_package, double_linked_list_package, file_package, sgf_utils_package;
use Ada.strings.unbounded, Ada.Text_IO.Unbounded_IO, metadata_package, file_package, sgf_utils_package;

procedure test_sgf is
   random_SGF : T_sgf;
   an_args_list, mkdirArgs, cdArgs : US_DBLL.T_double_link_list;
begin
   random_SGF := createSGF;
   an_args_list := US_DBLL.createList;
   
   put_line(pwd(random_SGF, an_args_list));
   
   mkdirArgs := US_DBLL.createList;
   US_DBLL.addFirst(mkdirArgs, To_Unbounded_String("/home/matthias"));
   mkdir(random_SGF, mkdirArgs);
   
   cdArgs := US_DBLL.createList;
   US_DBLL.addFirst(cdArgs, To_Unbounded_String("/home/matthias"));
   cd(random_SGF, cdArgs);
   
   Put_Line(pwd(random_SGF, an_args_list));
   
end test_sgf;
