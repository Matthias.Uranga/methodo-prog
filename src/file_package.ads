with Ada.Strings.Unbounded, Ada.Text_IO, Ada.Text_IO
  .Unbounded_IO, metadata_package, double_linked_list_package;
use Ada.Strings.Unbounded, Ada.Text_IO, Ada.Text_IO.Unbounded_IO,
  metadata_package;

package file_package is

   CHILDREN_LIST_EMPTY : exception;

   type T_File is private;
   type T_File_ptr is access T_File;

   function equals
     (l_file_ptr : T_File_ptr; r_file_ptr : T_File_ptr) return Boolean;
   function greaterThan
     (l_file_ptr : T_File_ptr; r_file_ptr : T_File_ptr) return Boolean;
   function lowerThan
     (l_file_ptr : T_File_ptr; r_file_ptr : T_File_ptr) return Boolean;

   package FILE_DBLL is new double_linked_list_package (T_File_ptr, equals,
      greaterThan, lowerThan);
   use FILE_DBLL;

-----------------------------------------------------------------------------
--                            GETTERS & SETTERS                            --
-----------------------------------------------------------------------------
   -- Name: get_Name
   -- Semantics: return file's name
   -- Parameters:
   --      F_File: the file you want the name of
   -- Return type: Unbounded_String
   -- Preconditions: /
   -- Postconditions: /
   function get_Name (F_File : in T_File_ptr) return Unbounded_String;

   -- Name: set_Name
   -- Semantics: sets a new name for the file
   -- Parameters:
   --      F_File: the file you want to change the name of
   --      new_Name: the new name for the file
   -- Preconditions: /
   -- Postconditions: /
   procedure set_Name (F_File : in T_File_ptr; new_Name : in Unbounded_String);

   -- Name: isDir
   -- Semantics: return wether its a directory or a file
   -- Parameters:
   --      F_File: the file you want to know wether it's a file or a directory
   -- Return type: Boolean
   -- Preconditions: /
   -- Postconditions: /
   function isDir (F_File : in T_File_ptr) return Boolean;

   -- Name: set_isDir
   -- Semantics: sets wether the file is a file or a directory
   -- Parameters:
   --      F_File: the file you want to change the status
   --      isItDir: the new file's status
   -- Preconditions: /
   -- Postconditions: /
   procedure set_isDir (F_File : in out T_File_ptr; isItDir : Boolean);

   -- Name: get_Parent
   -- Semantics: return the file's parent
   -- Parameters:
   --      F_File: the file you want the parent of
   -- Return type: T_File_ptr
   -- Preconditions: /
   -- Postconditions: /
   function get_Parent (F_File : in T_File_ptr) return T_File_ptr;

   -- Name: set_Parent
   -- Semantics: sets a new parent to a file
   -- Parameters:
   --      F_File: the file you want to change the parent
   --      new_Parent: the file's new parent
   -- Preconditions: /
   -- Postconditions: /
   procedure set_Parent (F_File : in T_File_ptr; new_Parent : in T_File_ptr);

   -- Name: get_Metadata
   -- Semantics: return the file's metadata
   -- Parameters:
   --      F_File: the file you want the metadata of
   -- Return type: T_File_ptr
   -- Preconditions: /
   -- Postconditions: /
   function get_Metadata (F_File : in T_File_ptr) return T_Metadata;

   -- Name: get_Children
   -- Semantics: return the file's children list
   -- Parameters:
   --      F_File: the file you want the children list of
   -- Return type: FILE_DBLL.T_double_link_list
   -- Preconditions: /
   -- Postconditions: /
   function get_Children
     (F_File : in T_File_ptr) return FILE_DBLL.T_double_link_list;

   -- Name: get_Child
   -- Semantics: return the file's child with the name F_childName
   -- Parameters:
   --      F_File: the file you want the children list of
   --      F_childName: the name of the child you want to get
   -- Return type: FILE_DBLL.T_double_link_list
   -- Preconditions: /
   -- Postconditions: /
   function get_Child
     (F_File : in T_File_ptr; F_childName: Unbounded_String) return T_File_ptr;

   -- Name: set_Children
   -- Semantics: sets a new children list to the file
   -- Parameters:
   --      F_File: the file you want to set a new children list
   --      new_Children: the new list of children
   -- Preconditions: /
   -- Postconditions: /
   procedure set_Children
     (F_File : in T_File_ptr; new_Children : FILE_DBLL.T_double_link_list);
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--                       CHILDREN ADDER AND DISPLAYER                      --
-----------------------------------------------------------------------------

   -- Name: add_Child
   -- Semantics: adds a child to the children list of the file, while maintaining an alphabetical order to the list
   -- Parameters:
   --      File_toAddChildTo: the file you want to add a children to
   --      child_toAdd: the child to add to the file
   -- Preconditions: /
   -- Postconditions: /
   procedure add_Child
     (File_toAddChildTo : in T_File_ptr; child_toAdd : in T_File_ptr);

   -- Name: displayChildren
   -- Semantics: displays a list of children
   -- Parameters:
   --      childrenToDisplay: the list of children to display
   -- Preconditions: /
   -- Postconditions: /
   procedure displayChildren
     (childrenToDisplay : in FILE_DBLL.T_double_link_list);
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--                         FILE AND DIRECTORY CREATORS                     --
-----------------------------------------------------------------------------
   -- Name: createFile
   -- Semantics: creates a file based on the information you put through parameters
   -- Parameters:
   --      F_File: the file you create
   --      new_Metadata: the metadata of the file you create
   --      new_Name: the name of the file you create
   -- Preconditions: /
   -- Postconditions: /
   procedure createFile
     (F_File       : in out T_File_ptr; new_Parent : T_File_ptr;
      new_Metadata :        T_Metadata;
      new_Name     :    Unbounded_String := To_Unbounded_String ("New file"));

   -- Name: createDir
   -- Semantics: creates a directory based on the information you put through parameters
   -- Parameters:
   --      F_File: the directory you create
   --      new_Metadata: the metadata of the directory you create
   --      new_Name: the name of the directory you create
   -- Preconditions: /
   -- Postconditions: /
   procedure createDir
     (F_File       : in out T_File_ptr; new_Parent : T_File_ptr;
      new_Metadata :        T_Metadata;
      new_Name :    Unbounded_String := To_Unbounded_String ("New directory"));
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

private

   type T_File is record
      name     : Unbounded_String;
      metadata : T_Metadata;
      isDir    : Boolean;
      parent   : T_File_ptr;
      children : FILE_DBLL.T_double_link_list;
   end record;

   -- Name: slotResearch
   -- Semantics: look for the slot of the child you want to add
   -- Parameters:
   --      children_List: the list of children you want to add a new child to
   --      child_toAdd: the child to add to the list
   -- Preconditions: /
   -- Postconditions: /
   procedure slotResearch
     (children_List : in out FILE_DBLL.T_double_link_list;
      child_toAdd   : in     T_File_ptr);

end file_package;
