with Ada.strings.unbounded, Ada.Text_IO, Ada.Text_IO.Unbounded_IO, metadata_package, double_linked_list_package, file_package;
use Ada.strings.unbounded, Ada.Text_IO, Ada.Text_IO.Unbounded_IO, metadata_package, file_package;


package sgf_utils_package is
   NO_CURRENT_FILE : Exception;
   
   DEFAULT_AUTHOR_NAME : constant Unbounded_String := To_Unbounded_String("muranga");
   DEFAULT_RIGHTS : constant String(1..get_MAX_RIGHTS_STRING_SIZE) := "rwx";
   DEFAULT_DIR_EXTENSION : constant Unbounded_String := To_Unbounded_String("");
   
   ROOT_NAME : constant Unbounded_String := To_Unbounded_String("/");
   CURRENT_DIR_NAME : constant Unbounded_String := To_Unbounded_String(".");
   PARENT_DIR_NAME : constant Unbounded_String := To_Unbounded_String("..");
   
   package US_DBLL is new double_linked_list_package(Unbounded_String, "=", ">", "<");
   use US_DBLL;   
     
   Type T_sgf is private;
   
   -- Name: pwd
   -- Semantics: prints current directory
   -- Parameters:
   --      F_sgf: the files system you're in
   --      F_args: the arguments list, an unbounded_strings double linked list
   -- Return type: Unbounded_String
   -- Preconditions: /
   -- Postconditions: /
   function pwd(F_sgf : in T_sgf; F_args : in out US_DBLL.T_double_link_list) return Unbounded_String;
   
         
   -- Name: mkdir
   -- Semantics: creates a directory in the file system
   -- Parameters:
   --      F_sgf: the files system you're in
   --      F_args: the arguments list, an unbounded_strings double linked list
   -- Preconditions: /
   -- Postconditions: /
   procedure mkdir(F_sgf : in T_sgf; F_args : in out US_DBLL.T_double_link_list);
   
   
   -- Name: cd
   -- Semantics: changes the F_sgf current directory to the one in the F_args list
   -- Parameters:
   --      F_sgf: the files system you're in
   --      F_args: the arguments list, an unbounded_strings double linked list
   -- Preconditions: /
   -- Postconditions: /
   procedure cd(F_sgf: in out T_sgf; F_args: in out US_DBLL.T_double_link_list);

   
   -- Name: createSGF
   -- Semantics: creates a file blank file system
   -- Parameters: /
   -- Return type: T_sgf
   -- Preconditions: /
   -- Postconditions: /
   function createSGF return T_sgf;
   
   -- Name: split_US
   -- Semantics: splits an unbounded string on char_toSplitOn
   -- Parameters: 
   --      string_toSplit: the unbounded string to split in pieces
   --      char_toSplitOn: the character that you have to split the string on
   -- Return type: US_DBLL.T_double_link_list
   -- Preconditions: /
   -- Postconditions: /
   function split_US(string_toSplit : in Unbounded_String; char_toSplitOn : in Character) return US_DBLL.T_double_link_list;
        
   -- Name: display_US_list
   -- Semantics: displays a list of Unbounded Strings
   -- Parameters: 
   --      F_list_to_display: the list to display
   -- Preconditions: /
   -- Postconditions: /
   procedure display_US_list(F_list_to_display : in US_DBLL.T_double_link_list);
   
private
   Type T_sgf is record
      root : T_File_ptr;
      current : T_File_ptr;
   end record;
   
   -- Name: get_Current
   -- Semantics: returns current directory
   -- Parameters:
   --      F_sgf: the file system you want the current directory
   -- Return type: T_File_ptr
   -- Preconditions: /
   -- Postconditions: /
   function get_Current(F_sgf : in T_sgf) return T_File_ptr;
                
   -- Name: get_AbsolutePath
   -- Semantics: returns the absolute path, inside of F_sgf, of the F_file you put in parameters
   -- Parameters:
   --      F_sgf: the file system in which you are working
   --      F_file: the file you want the absolute path of
   -- Return type: Unbounded_String
   -- Preconditions: /
   -- Postconditions: /
   function get_AbsolutePath(F_sgf : in T_sgf; F_file : in T_File_ptr) return Unbounded_String;
   
   -- Name: get_Directory
   -- Semantics: returns the file pointer on the child that has the name F_childName, and is one of F_file children, inside F_sgf
   -- Parameters:
   --      F_sgf: the file system in which you are working
   --      F_file: the file you want to get the child 
   --      F_childName: the name of the child you want to get
   -- Return type: T_File_ptr
   -- Preconditions: /
   -- Postconditions: /
   function get_Directory(F_sgf: in T_sgf; F_file: in T_File_ptr; F_childName: Unbounded_String) return T_File_ptr;
   
   -- Name: get_Paths
   -- Semantics: returns a list of Ubounded strings, which are all file/directory names that were in string_toPutTo_list
   -- Parameters:
   --      string_toPutTo_list: the string of a path, that you want in list of all file/directory names in this path
   -- Return type: US_DBLL.T_double_link_list
   -- Preconditions: /
   -- Postconditions: /
   function get_Paths(string_toPutTo_list : in String) return US_DBLL.T_double_link_list;
   
   
end sgf_utils_package;
