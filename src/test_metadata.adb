with metadata_package, Ada.strings.unbounded, Ada.Text_IO.Unbounded_IO;
use metadata_package, Ada.strings.unbounded, Ada.Text_IO.Unbounded_IO;

procedure test_metadata is
   test_metadata_firstAuthor : Unbounded_String;
   test_metadata_secondAuthor : Unbounded_String;
   test_metadata_rights : String(1..get_MAX_RIGHTS_STRING_SIZE);
   test_metadata_extension : String(1..get_MAX_EXTENSION_STRING_SIZE);
   test_metadata_size : Natural;
   test_metadata_global : T_Metadata;
begin
   test_metadata_firstAuthor := ada.strings.Unbounded.To_Unbounded_String("Charles Dupont");
   test_metadata_secondAuthor := ada.strings.Unbounded.To_Unbounded_String("Marie Louis");
 
   set_Author(test_metadata_global, test_metadata_firstAuthor);
   put(get_Author(test_metadata_global));
   
 
   test_metadata_rights := "rw-";
   set_Rights(test_metadata_global, test_metadata_rights);
   
   test_metadata_size := 20;
   set_Size(test_metadata_global, test_metadata_size);
   
   test_metadata_extension := ".txt";
   set_Extension(test_metadata_global, test_metadata_extension);
end test_metadata;
