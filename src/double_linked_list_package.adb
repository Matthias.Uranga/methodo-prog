package body double_linked_list_package is

-----------------------------------------------------------------------------
--                 MOVEMENTS AND "CHECKERS" ON NEXT & PREVIOUS             --
-----------------------------------------------------------------------------
   function got_Next (F_dbl_list : in T_double_link_list) return Boolean is
   begin
      if get_Content (F_dbl_list) = get_Last_Content (F_dbl_list) then
         -- if current list element is last, it has no next, returns False
         return False;
      else
         -- if it isn't last, it has a next, returns True
         return True;
      end if;
   exception
      -- by default when the list is empty, it has no next, returns False
      when LIST_EMPTY =>
         return False;
   end got_Next;

   function got_Previous (F_dbl_list : in T_double_link_list) return Boolean is
   begin
      if get_Content (F_dbl_list) = get_First_Content (F_dbl_list) then
         -- if current list element is first, it has no previous, returns False
         return False;
      else
         -- if it isn't last, it has a previous, returns True
         return True;
      end if;
   exception
      -- by default when the list is empty, it has no previous, returns False
      when LIST_EMPTY =>
         return False;
   end got_Previous;

   ----------------------------------------------------------------------------

   procedure goTo_Next (F_dbl_list : in out T_double_link_list) is
   begin
      if got_Next (F_dbl_list) then
         -- if current list element has a next, goes to next
         go_To (F_dbl_list, get_Next_Content (F_dbl_list));
      else
         -- if it has no next it raises NO_ELEMENT_AFTER as this procedure shouldn't be called
         raise NO_ELEMENT_AFTER;
      end if;
   end goTo_Next;

   procedure goTo_Previous (F_dbl_list : in out T_double_link_list) is
   begin
      if got_Previous (F_dbl_list) then
         -- if current list element has a previous, goes to previous
         go_To (F_dbl_list, get_Previous_Content (F_dbl_list));
      else
         -- if its has no previous, raises NO_ELEMENT_BEFORE as this procedure shouldn't be called
         raise NO_ELEMENT_BEFORE;
      end if;
   end goTo_Previous;

-----------------------------------------------------------------------------

   procedure go_To
     (F_dbl_list               : in out T_double_link_list;
      F_element_toGoTo_content :        T_generic_content)
   is
      current_element_at_function_call : T_double_link_list := F_dbl_list;
      isElementFound                   : Boolean            := False;
   begin
      if F_element_toGoTo_content = get_First_Content (F_dbl_list) then
         -- if the element you want to go to is the first
         while got_Previous (F_dbl_list) loop
            -- goes back to the first element that has no previous, the first one of the list
            F_dbl_list.current_list_element :=
              F_dbl_list.current_list_element.previous;
         end loop;

      elsif F_element_toGoTo_content = get_Last_Content (F_dbl_list) then
         -- if the element you want to go to is the last
         while got_Next (F_dbl_list) loop
   -- goes front to the last element that has a next, the last one of the list
            F_dbl_list.current_list_element :=
              F_dbl_list.current_list_element.next;
         end loop;

      else
         -- if its any element in the list
         while got_Next (F_dbl_list) and then not isElementFound loop
            -- starts by going from the current element to the end of the list
            if get_Content (F_dbl_list) /= F_element_toGoTo_content then
               -- if the current element is not the element you want to go to
               -- you go to its next element
               F_dbl_list.current_list_element :=
                 F_dbl_list.current_list_element.next;
            else
         -- the element is found, and current is set to it, you stop the loop
               isElementFound := True;
            end if;
         end loop;

         if not isElementFound then
-- if the element is not found between the current at call and the last element
            F_dbl_list := current_element_at_function_call;
            -- you reset the current to its value at call
            while got_Previous (F_dbl_list) and then not isElementFound loop
               -- and following the same way, you look for the element, from the current element at call to the first
               if get_Content (F_dbl_list) /= F_element_toGoTo_content then
               -- if the current element is not the element you want to go to
               -- you go to its previous element
                  F_dbl_list.current_list_element :=
                    F_dbl_list.current_list_element.previous;
               else
         -- the element is found, and current is set to it, you stop the loop
                  isElementFound := True;
               end if;
            end loop;
         else
            null;
         end if;
      end if;
   end go_To;
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--                            GETTERS ON ALL ELEMENTS                      --
-----------------------------------------------------------------------------

   function get_Next_Content
     (F_dbl_list : in T_double_link_list) return T_generic_content
   is
   begin
      if got_Next (F_dbl_list) then
         -- if the current list element has a next, returns it
         return F_dbl_list.current_list_element.next.content;
      else
         -- if it doesn't, returns the current element, as it's the last
         return F_dbl_list.current_list_element.content;
      end if;
   end get_Next_Content;

   function get_Previous_Content
     (F_dbl_list : in T_double_link_list) return T_generic_content
   is
   begin
      if got_Previous (F_dbl_list) then
         -- if the current list element has a previous, returns it
         return F_dbl_list.current_list_element.previous.content;
      else
         -- if it doesn't, returns the current element, as it's the first
         return F_dbl_list.current_list_element.content;
      end if;
   end get_Previous_Content;

   function get_First_Content
     (F_dbl_list : in T_double_link_list) return T_generic_content
   is
   begin
      if get_Length (F_dbl_list) = 0 then
   -- if list is empty, raises LIST_EMPTY, as this function shouldn't be called
         raise LIST_EMPTY;
      else
         -- if it's not empty, returns the first element's value
         return F_dbl_list.first.content;
      end if;
   end get_First_Content;

   function get_Last_Content
     (F_dbl_list : in T_double_link_list) return T_generic_content
   is
   begin
      if get_Length (F_dbl_list) = 0 then
   -- if list is empty, raises LIST_EMPTY, as this function shouldn't be called
         raise LIST_EMPTY;
      else
         -- if it's not empty, returns the last element's value
         return F_dbl_list.last.content;
      end if;
   end get_Last_Content;

   function get_Content
     (F_dbl_list : in T_double_link_list) return T_generic_content
   is
   begin
      if get_Length (F_dbl_list) = 0 then
   -- if list is empty, raises LIST_EMPTY, as this function shouldn't be called
         raise LIST_EMPTY;
      else
         -- if it's not empty, returns the current element's value
         return F_dbl_list.current_list_element.content;
      end if;
   end get_Content;
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--                         CREATER, ADDERS AND REMOVER                     --
-----------------------------------------------------------------------------
   function createList return T_double_link_list is
   begin
      return T_double_link_list'(null, null, null, 0);
   end createList;

   procedure addLast
     (F_dbl_list           : in out T_double_link_list;
      F_newElement_content : in     T_generic_content)
   is
   begin
      if isEmpty (F_dbl_list) then
         addToEmptyList (F_dbl_list, F_newElement_content);
      else
         -- if list is not empty, sets current list as a list element with no next element, but the preceding last element as its previous, and the value the procedure caller asked
         F_dbl_list.current_list_element :=
           new T_list_element'(null, F_dbl_list.last, F_newElement_content);
         -- sets preceding last element's next as the new last, and list's last element as the new last aswell
         F_dbl_list.last.next := F_dbl_list.current_list_element;
         F_dbl_list.last      := F_dbl_list.current_list_element;
      end if;
      -- don't forget to add 1 to the list's length
      F_dbl_list.length := get_Length (F_dbl_list) + 1;
   end addLast;

   procedure addFirst
     (F_dbl_list           : in out T_double_link_list;
      F_newElement_content : in     T_generic_content)
   is
   begin
      if isEmpty (F_dbl_list) then
         addToEmptyList (F_dbl_list, F_newElement_content);
      else
         -- if list is not empty, sets current list as a list element with no previous element, but the preceding first element as its next, and the value the procedure caller asked
         F_dbl_list.current_list_element :=
           new T_list_element'(F_dbl_list.first, null, F_newElement_content);
         -- sets preceding first element's previous as the new first, and list's first element as the new first aswell
         F_dbl_list.first.previous := F_dbl_list.current_list_element;
         F_dbl_list.first          := F_dbl_list.current_list_element;
      end if;
      -- don't forget to add 1 to the list's length
      F_dbl_list.length := get_Length (F_dbl_list) + 1;
   end addFirst;

   procedure addAfter
     (F_dbl_list           : in out T_double_link_list;
      F_newElement_content : in     T_generic_content)
   is
   begin
      if isEmpty (F_dbl_list) then
         addToEmptyList (F_dbl_list, F_newElement_content);
      elsif F_dbl_list.current_list_element = F_dbl_list.last then
         -- if current list element is last, sets current element as a list element with no next element, but the preceding last element as its previous, and the value the procedure caller asked
         F_dbl_list.current_list_element.next :=
           new T_list_element'
             (null, F_dbl_list.current_list_element, F_newElement_content);
         -- sets list's last element as the new element
         F_dbl_list.last := F_dbl_list.current_list_element.next;
      else
         -- if current list element is not last, sets current's next element as a list element with current's next as next, current as previous, and the value the procedure caller asked
         F_dbl_list.current_list_element.next :=
           new T_list_element'
             (F_dbl_list.current_list_element.next,
              F_dbl_list.current_list_element, F_newElement_content);
         -- sets the current's next element's previous, as the new element
         F_dbl_list.current_list_element.next.next.previous :=
           F_dbl_list.current_list_element.next;
      end if;
      -- don't forget to add 1 to the list's length
      F_dbl_list.length := get_Length (F_dbl_list) + 1;
   end addAfter;

   procedure addBefore
     (F_dbl_list           : in out T_double_link_list;
      F_newElement_content : in     T_generic_content)
   is
   begin
      if isEmpty (F_dbl_list) then
         -- if list is empty, calls addToEmptyList
         addToEmptyList (F_dbl_list, F_newElement_content);
      elsif F_dbl_list.current_list_element = F_dbl_list.first then
         -- if current list element is first, sets current element as a list element with no previous element, but the preceding first element as its next, and the value the procedure caller asked
         F_dbl_list.current_list_element.previous :=
           new T_list_element'
             (F_dbl_list.current_list_element, null, F_newElement_content);
         -- sets list's first element as the new element
         F_dbl_list.first := F_dbl_list.current_list_element.previous;
      else
         -- if current list element is not first, sets current's previous element as a list element with current's previous as previous, current as next, and the value the procedure caller asked
         F_dbl_list.current_list_element.previous :=
           new T_list_element'
             (F_dbl_list.current_list_element,
              F_dbl_list.current_list_element.previous, F_newElement_content);
         -- sets the current's previous element's next, as the new element
         F_dbl_list.current_list_element.previous.previous.next :=
           F_dbl_list.current_list_element.previous;
      end if;
      -- don't forget to add 1 to the list's length
      F_dbl_list.length := get_Length (F_dbl_list) + 1;
   end addBefore;

   procedure removeElement
     (F_dbl_list                : in out T_double_link_list;
      F_elementToRemove_content : in     T_generic_content)
   is
      current_Element_at_call : T_double_link_list := F_dbl_list;
   begin
      if get_Length (F_dbl_list) = 1 then
         -- if list has only 1 element, sets an empty list
         F_dbl_list.first                := null;
         F_dbl_list.last                 := null;
         F_dbl_list.current_list_element := null;
      elsif F_elementToRemove_content = get_First_Content (F_dbl_list) then
      -- if element to remove is the first, sets current element to the first
         go_To (F_dbl_list, get_First_Content (F_dbl_list));
         -- sets current's next as first element, and his previous to null
         F_dbl_list.first := F_dbl_list.current_list_element.next;
         F_dbl_list.current_list_element.next.previous := null;

      elsif F_elementToRemove_content = get_Last_Content (F_dbl_list) then
         -- if element to remove is the last, sets current element to the last
         go_To (F_dbl_list, get_Last_Content (F_dbl_list));
         -- sets current's previous as last element, and his next to null
         F_dbl_list.last := F_dbl_list.current_list_element.previous;
         F_dbl_list.current_list_element.previous.next := null;
      else
         -- if element to remove is any element, sets current as this element
         go_To (F_dbl_list, F_elementToRemove_content);
         -- sets current previous' next as current's next, and current next's as current's previous
         F_dbl_list.current_list_element.previous.next :=
           F_dbl_list.current_list_element.next;
         F_dbl_list.current_list_element.next.previous :=
           F_dbl_list.current_list_element.previous;
         -- sets current as removed's previous, could be his next
         go_To (F_dbl_list, get_Previous_Content (current_Element_at_call));

      end if;
      -- don't forget to remove 1 to the list's length
      F_dbl_list.length := get_Length (F_dbl_list) - 1;
   end removeElement;

-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--                      UTILS : LENGTH, EMPTY CHECKER                      --
-----------------------------------------------------------------------------
   function get_length (F_dbl_list : in T_double_link_list) return Natural is
   begin
      return F_dbl_list.length;
   end get_length;

   function isEmpty (F_dbl_list : in T_double_link_list) return Boolean is
   begin
      if get_length (F_dbl_list) = 0 then
         return True;
      else
         return False;
      end if;
   end isEmpty;

   procedure empty (F_dbl_list : in out T_double_link_list) is
   begin
      F_dbl_list.first                := null;
      F_dbl_list.last                 := null;
      F_dbl_list.current_list_element := null;
      F_dbl_list.length               := 0;
   end empty;
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--                      PRIVATE FUNCTIONS & PROCEDURES                     --
-----------------------------------------------------------------------------

   procedure addToEmptyList
     (F_dbl_list           : in out T_double_link_list;
      F_newElement_content : in     T_generic_content)
   is
   begin
      -- if list is empty, sets current list element as a list element with no next or previous element, and the value the procedure caller asked
      F_dbl_list.current_list_element :=
        new T_list_element'(null, null, F_newElement_content);
      -- sets list's first and last element as the current, as he is the only one in it
      F_dbl_list.first := F_dbl_list.current_list_element;
      F_dbl_list.last  := F_dbl_list.current_list_element;
   end addToEmptyList;
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------
end double_linked_list_package;
