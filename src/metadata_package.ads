with Ada.strings.unbounded, Ada.Text_IO.Unbounded_IO;
use Ada.strings.unbounded, Ada.Text_IO.Unbounded_IO;
package metadata_package is

   Type T_Metadata is private;
   
   function get_Author(F_Metadata : in T_Metadata) return Unbounded_String;
   procedure set_Author(F_Metadata : in out T_Metadata; new_Author : in Unbounded_String);
   
   function get_Rights(F_Metadata : in T_Metadata) return String;
   procedure set_Rights(F_Metadata : in out T_Metadata; new_Rights : in String);

   function get_Size(F_Metadata : in T_Metadata) return integer;
   procedure set_Size(F_Metadata : in out T_Metadata; new_Size : in integer);
   
   function get_Extension(F_Metadata : in T_Metadata) return Unbounded_String;
   procedure set_Extension(F_Metadata : in out T_Metadata; new_Extension : in Unbounded_String);
   
   function get_MAX_RIGHTS_STRING_SIZE return Natural;
   function get_MAX_EXTENSION_STRING_SIZE return Natural;
   
   function createMeta(F_author : Unbounded_String; F_rights : String; F_size : Natural; F_extension : Unbounded_String) return T_Metadata;

private
   MAX_RIGHTS_STRING_SIZE : constant Natural := 3;
   MAX_EXTENSION_STRING_SIZE : constant Natural := 3;

   Type T_Metadata is record 
      author : Unbounded_String;
      rights : String(1..MAX_RIGHTS_STRING_SIZE);
      size : Natural;
      extension : Unbounded_String;
   end record;
   
end metadata_package;
