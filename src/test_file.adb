with Ada.strings.unbounded, Ada.Text_IO.Unbounded_IO, Ada.Text_IO, metadata_package, file_package;
use Ada.strings.unbounded, Ada.Text_IO.Unbounded_IO, Ada.Text_IO, metadata_package, file_package;

procedure test_file is
   AUTHOR_NAME : constant Unbounded_String := To_Unbounded_String("muranga");
  
   --currentPath : FILE_DBLL.T_double_link_list;

   root : T_File_ptr;
   root_metadata : T_Metadata;
   
   home : T_File_ptr;
   home_metadata : T_Metadata;
        
   etc : T_File_ptr;
   etc_metadata : T_Metadata;
   
   var : T_File_ptr;
   var_metadata : T_Metadata;
   
   test : T_File_ptr;
   test_metadata : T_Metadata;
   
         
begin    
   
   set_Author(root_metadata, AUTHOR_NAME);
   set_Extension(root_metadata, To_Unbounded_String(""));
   set_Rights(root_metadata, "rwx");
   set_Size(root_metadata, 30);
   createDir(F_File => root, new_Parent => null, new_Metadata => root_metadata, new_Name => To_Unbounded_String("/"));
   put_line("root author : " & get_Author(get_Metadata(root)));
   put_line("root rights : " & get_Rights(get_Metadata(root))); 
   put_line("root size : " & natural'image(get_Size(get_Metadata(root))));
   put_line("root children : ");
   displayChildren(get_Children(root));
   put_line("");
   
   set_Author(home_metadata, AUTHOR_NAME);
   set_Extension(home_metadata, To_Unbounded_String(""));
   set_Rights(home_metadata, "rwx");
   set_Size(home_metadata, 50);
   createDir(F_File => home, new_Parent => root, new_Metadata => home_metadata, new_Name => To_Unbounded_String("home"));
   add_Child(root, home);
   put_line("root children after adding home : ");
   displayChildren(get_Children(root));
   
   set_Author(etc_metadata, AUTHOR_NAME);
   set_Extension(etc_metadata, To_Unbounded_String(""));
   set_Rights(etc_metadata, "rwx");
   set_Size(etc_metadata, 20);
   createDir(F_File => etc, new_Parent => root, new_Metadata => etc_metadata, new_Name => To_Unbounded_String("etc"));
   add_Child(root, etc);
   put_line("root children after adding etc : ");
   displayChildren(get_Children(root));
      
   set_Author(var_metadata, AUTHOR_NAME);
   set_Extension(var_metadata, To_Unbounded_String(""));
   set_Rights(var_metadata, "rwx");
   set_Size(var_metadata, 20);
   createDir(F_File => var, new_Parent => root, new_Metadata => var_metadata, new_Name => To_Unbounded_String("var"));
   add_Child(root, var);   
   put_line("root children after adding var : ");
   displayChildren(get_Children(root));

   
   set_Author(test_metadata, AUTHOR_NAME);
   set_Extension(test_metadata, To_Unbounded_String(".txt"));
   set_Rights(test_metadata, "rwx");
   set_Size(test_metadata, 20);
   createDir(F_File => test, new_Parent => home, new_Metadata => test_metadata, new_Name => To_Unbounded_String("test"));
   add_Child(home, test);
   Put_Line("home children after adding test.txt : ");
   displayChildren(get_Children(home));
   
   put_line("end of tests");
   
end test_file;
