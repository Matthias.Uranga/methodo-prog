with Ada.Characters.Latin_1, Ada.strings.unbounded, Ada.Text_IO, Ada.Text_IO.Unbounded_IO, sgf_utils_package;
use Ada.Characters.Latin_1, Ada.strings.unbounded, Ada.Text_IO, Ada.Text_IO.Unbounded_IO, sgf_utils_package;

with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;

procedure Sgf_Main is

    system: T_sgf := createSGF;
    args: US_DBLL.T_double_link_list := US_DBLL.createList;
    command: Unbounded_String;
    isMenuActive, doesUserWantToExit: Boolean := False;
    dumpChar: Character;

    function menu return String is
        choice: Natural;
        badChoice: Boolean := False;
        command: Unbounded_String;
    begin
        loop
            begin
                Put_Line(ESC & "[2J");
                if badChoice then
                    Put_Line("Bad choice, retry.");
                end if;
                Put_Line("(1) Print working directory");
                Put_Line("(2) Create directory");
                Put_Line("(3) Change directory");
                Put_Line("(4) Exit");
                Put("Choice: ");
                Choice := Natural'value(Get_Line);
                exception
                    when CONSTRAINT_ERROR => badChoice := True;
            end;
            exit when Choice >= 1 and Choice <= 4;
            badChoice := True;
        end loop;

        case Choice is
            when 1 =>
                return "pwd";
            when 2 =>
                Put("Path of new directory: ");
                return "mkdir " & Get_Line;
            when 3 =>
                Put("Change current path to path: ");
                return "cd " & Get_Line;
            when 4 =>
                isMenuActive := False;
                return "";
            when others =>
                return "";
        end case;
    end menu;

    procedure prompt(F_system: in out T_sgf; F_args: in out US_DBLL.T_double_link_list; isMenuActive: in out Boolean) is
        command: Unbounded_String;
    begin
      command := US_DBLL.get_First_Content(F_args);
      US_DBLL.removeElement(F_args, command);
      if command = "pwd" then
         Put_Line(pwd(F_system, F_args));
      elsif command = "mkdir" then
         mkdir(F_system, F_args);
      elsif command = "cd" then
         cd(F_system, F_args);
      elsif command = "menu" then
         isMenuActive := True;
      elsif command = "exit" then
         doesUserWantToExit := True;
      else
         Put_Line(To_String(command) & ": command was not found.");
      end if;
    end prompt;
begin
   while True loop
      US_DBLL.empty(args);
      if isMenuActive then
         command := To_Unbounded_String(menu);
         if command /= "" then
            args := split_US(command, ' ');
            Put_Line(US_DBLL.get_First_Content(args));
            prompt(system, args, isMenuActive);
            New_Line;
            Put_Line("Press enter key to continue...");
            Get_Immediate(dumpChar);
         else
            isMenuActive := False;
         end if;
      else
         Put("$ > ");
         args := split_US(To_Unbounded_String(Get_Line), ' ');
         if US_DBLL.get_Length(args) /= 0 then
            command := US_DBLL.get_First_Content(args);
            prompt(system, args, isMenuActive);
            exit when doesUserWantToExit;
         end if;
      end if;
   end loop;

end Sgf_Main;
