package body sgf_utils_package is

   
-----------------------------------------------------------------------------
--                               SGF COMMANDS                              --
-----------------------------------------------------------------------------
   function pwd(F_sgf : in T_sgf; F_args : in out US_DBLL.T_double_link_list)  return Unbounded_String is
   begin
      if US_DBLL.get_Length(F_args) /= 0 then
         -- if there are any arguments in the call of pwd, returns "Invalid number of arguments !" as it shouldn't be called without any argument
         return To_Unbounded_String("Invalid number of arguments !");
      else
         -- if there are no arguments, returns the absolute path of the current directory
         return get_AbsolutePath(F_sgf, get_Current(F_sgf));
      end if;      
   end pwd;
   
   
   procedure mkdir(F_sgf : in T_sgf; F_args : in out US_DBLL.T_double_link_list) is
      tree_depth_iterator : T_File_ptr := F_sgf.current;
      new_dir_name : Unbounded_String;
      new_dir_meta : T_Metadata;
      new_dir : T_File_ptr;
      dirs_list : US_DBLL.T_double_link_list;
   begin
      if US_DBLL.get_Length(F_args) /= 1 then
         -- if there is more or less than 1 argument, puts "Invalid number of arguments !" as mkdir needs exactly one argument to be called
         Put_Line("Invalid number of arguments !");
      else
         dirs_list := get_Paths(To_String(get_First_Content(F_args)));
         -- look for the last element that exists in F_sgf (the argument can be /home/matthias/toto, where matthias and toto aren't in the F_sgf)
         for i in 1..get_Length(F_args) loop
            tree_depth_iterator := get_Directory(F_sgf, tree_depth_iterator, get_Content(dirs_list));
            goTo_Next(dirs_list);
         end loop;       
         
         loop
            new_dir_name := get_Content(dirs_list);                        
            new_dir_meta := createMeta(DEFAULT_AUTHOR_NAME, DEFAULT_RIGHTS, 50, DEFAULT_DIR_EXTENSION);
            -- each loop iteration you create a new dir with current dirs_list's element as name, and default metadata values
            createDir(new_dir, tree_depth_iterator, new_dir_meta, new_dir_name);
            -- then you add it to the current tree level children
            add_Child(tree_depth_iterator, new_dir);
            -- and you go deeper in the tree, to eventually the last element of the path (F_args)
            tree_depth_iterator := new_dir;
            exit when not got_Next(dirs_list);
            -- if there are no more directories in the list from the path of F_args, you stop the loop, else you go to the next
            goTo_Next(dirs_list);
         end loop;
      end if;      
   end mkdir;
    
   
   procedure cd(F_sgf: in out T_sgf; F_args: in out US_DBLL.T_double_link_list) is
        tree_depth_iterator: T_File_ptr := F_sgf.current;
        dirs_list: US_DBLL.T_double_link_list;
   begin
      if get_Length(F_args) /= 1 then
         -- if there is more or less than 1 argument, puts "Invalid number of arguments !" as cd needs exactly one argument to be called
           Put_Line("Invalid number of arguments !");
      end if;

      dirs_list := get_Paths(To_String(get_Content(F_args)));
      -- you get a list of directory names in dirs_list, corresponding to the path you get in F_args
      loop
         -- then you iterate on that list, to get the most further of them, based on the tree depth
         tree_depth_iterator := get_Directory(F_sgf, tree_depth_iterator, get_Content(dirs_list));
         exit when not got_Next(dirs_list);
         goTo_Next(dirs_list);
      end loop;
      -- and you set the current directory to the deeper element of the list you get at function call
      F_sgf.current := tree_depth_iterator;
   end cd;
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------
    
-----------------------------------------------------------------------------
--                 UTILS FUNCTIONS : createSGF & split_US                  --
-----------------------------------------------------------------------------
   
   function createSGF return T_sgf is
      root_dir : T_File_ptr;
      root_dir_meta : T_Metadata;
      createdSGF : T_sgf;
      
   begin
      root_dir_meta := createMeta(DEFAULT_AUTHOR_NAME, DEFAULT_RIGHTS, 150, DEFAULT_DIR_EXTENSION);
      createDir(root_dir, null, root_dir_meta, To_Unbounded_String("/"));
      createdSGF.root := root_dir;
      createdSGF.current := root_dir;
      return createdSGF;
   end createSGF;
   
      
   function split_US(string_toSplit : in Unbounded_String; char_toSplitOn : in Character) return US_DBLL.T_double_link_list is
      argument : Unbounded_String;
      string_length : Integer;
      low, high : Integer := 0;
      args_list : US_DBLL.T_double_link_list := createList;
      splitterChar_found : Boolean := False;
   begin
      string_length := Length(string_toSplit);
      for i in 1..string_length loop
         if Element(string_toSplit, i) = char_toSplitOn or (splitterChar_found and then i = string_length) then
            splitterChar_found := True;
            if high = 0 then
               high := i;
            elsif i = string_length then
               low := high;
               high := string_length+1;
            else
               low := high;
               high := i;
            end if;
            Unbounded_Slice(string_toSplit, argument, low+1, high-1);
            addLast(args_list, argument);
         elsif not splitterChar_found and then i = string_length then
            addLast(args_list, string_toSplit);
         end if;
      end loop;
      return args_list;
   end split_US;      
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
--                      PRIVATE FUNCTIONS & PROCEDURES                     --
-----------------------------------------------------------------------------
   
   function get_Current(F_sgf : in T_sgf) return T_File_ptr is
   begin
      if F_sgf.current /= null then
         -- if the current directory is set, returns it
         return F_sgf.current;
      else
         -- if it isn't set, raise NO_CURRENT_FIL as get_Current shouldn't have been called
         raise NO_CURRENT_FILE;
      end if;
   end get_Current;
                  
   function get_AbsolutePath(F_sgf : in T_sgf; F_file : in T_File_ptr) return Unbounded_String is
   begin
      if get_Parent(F_File) /= null then
         -- if F_file isn't your root, returns the absolute path of your file's parent, your file name and "/" 
         return get_AbsolutePath(F_sgf, get_Parent(F_File)) & get_Name(F_File) & To_Unbounded_String("/");
      else
         -- if F_file is your root, returns root name
         return get_Name(F_sgf.root);
      end if;      
   end get_AbsolutePath;
   
   
   function get_Directory(F_sgf: in T_sgf; F_file: in T_File_ptr; F_childName: Unbounded_String) return T_File_ptr is
   begin
      if F_childName = ROOT_NAME then
         return F_sgf.root;
      elsif F_childName = PARENT_DIR_NAME and get_Name(F_File) = ROOT_NAME then
         return F_file;
      elsif F_childName = PARENT_DIR_NAME then
         return get_Parent(F_File);
      elsif F_childName = CURRENT_DIR_NAME then
         return F_File;
      else
         return get_Child(F_file, F_childName);
      end if;              
   end get_Directory;
   
   
   function get_Paths(string_toPutTo_list : String) return US_DBLL.T_double_link_list is
      paths_list : US_DBLL.T_double_link_list := createList;
   begin
      if string_toPutTo_list'Length >= 1 then
            if string_toPutTo_list(string_toPutTo_list'First) = '/' then
                paths_list := split_US(To_Unbounded_String(string_toPutTo_list(string_toPutTo_list'First + 1..string_toPutTo_list'Last)), '/');
                addFirst(paths_list, To_Unbounded_String("/"));
                go_To(paths_list, get_First_Content(paths_list));
            else
                paths_list := split_US(To_Unbounded_String(string_toPutTo_list), '/');
            end if;
            return paths_list;
        else
            addAfter(paths_list, To_Unbounded_String(""));
        end if;
        return paths_list;

   end get_Paths;
   
   
   procedure display_US_list(F_list_to_display : in US_DBLL.T_double_link_list) is
      listIterator : US_DBLL.T_double_link_list := F_list_to_display;
   begin      
      if isEmpty(listIterator) then
         -- if list is empty, simply put that it is 
         put("List empty");
      else
         -- if list is not empty, make sure to start at the first element of the children list
         go_To(listIterator, get_First_Content(listIterator));
         loop                 
            -- if current child is a directory, only put his name, followed by a comma if its not the last, else exit the loop;
            put(get_Content(listIterator));
            exit when not got_Next(listIterator);
            goTo_Next(listIterator);
            put(", ");            
         end loop;        
      end if;
      New_Line;
   end display_US_list;   
-----------------------------------------------------------------------------
--                                                                         --
-----------------------------------------------------------------------------         
end sgf_utils_package;
