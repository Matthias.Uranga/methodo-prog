with Ada.Strings.Unbounded, Ada.Text_IO, Ada.Text_IO
  .Unbounded_IO, double_linked_list_package;
use Ada.Strings.Unbounded, Ada.Text_IO, Ada.Text_IO.Unbounded_IO;

procedure test_dbll is
   package Int_DBLL is new double_linked_list_package (Integer, "=", ">", "<");
   use Int_DBLL;

   a_list : Int_DBLL.T_double_link_list;

   procedure displayList (F_list : in out Int_DBLL.T_double_link_list) is
   begin
      if isEmpty (F_list) then
         Put_Line ("Empty list, nothing to display");
      else
         go_To (F_list, get_First_Content (a_list));
         loop
            Put_Line (Integer'Image (get_Content (F_list)));
            exit when not got_Next (F_list);
            goTo_Next (F_list);
         end loop;
      end if;
   end displayList;

begin
   Put ("Testing createList: ");
   a_list := Int_DBLL.createList;
   if isEmpty (a_list) then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");

   Put ("Testing addFirst with empty List: ");
   addFirst (a_list, 3);
   if get_First_Content (a_list) = 3 and
     get_Last_Content (a_list) = get_First_Content (a_list) and
     get_Content (a_list) = get_First_Content (a_list) and not isEmpty (a_list)
   then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");

   Put ("Testing removeElement on first: ");
   removeElement
     (a_list,
      3); --Could be called with : removeElement(a_list, get_First_Content)
   if isEmpty (a_list) then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");

   Put ("Testing addAfter with empty list: ");
   addAfter (a_list, 4);
   if get_First_Content (a_list) = 4 and
     get_Last_Content (a_list) = get_First_Content (a_list) and
     get_Content (a_list) = get_First_Content (a_list) and not isEmpty (a_list)
   then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");

   Put ("Testing addFirst with not empty list: ");
   addFirst (a_list, 40);
   if get_First_Content (a_list) = 40 and then not isEmpty (a_list) then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");

   Put ("Testing removeElement on last: ");
   removeElement (a_list, get_Last_Content (a_list));
   if get_Last_Content (a_list) = get_First_Content (a_list)
     and not isEmpty (a_list)
   then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");

   Put ("Testing empty: ");
   empty (a_list);
   if isEmpty (a_list) then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");

   Put ("Testing addBefore with empty list: ");
   addBefore (a_list, 10);
   if get_First_Content (a_list) = 10 and
     get_Last_Content (a_list) = get_First_Content (a_list) and
     get_Content (a_list) = get_First_Content (a_list) and not isEmpty (a_list)
   then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");

   Put ("Testing addLast with not empty list: ");
   addLast (a_list, 11);
   if get_Last_Content (a_list) = 11 and then not isEmpty (a_list) then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");

   Put ("Testing addAfter on last: ");
   go_To(a_list, get_Last_Content(a_list));
   addAfter (a_list, 12);
   if get_Last_Content (a_list) = 12 and then not isEmpty (a_list) then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");

   Put ("Testing addAfter on any: ");
   go_To(a_list, get_Last_Content(a_list));
   go_To(a_list, get_Previous_Content(a_list));
   addAfter (a_list, 13);
   go_To(a_list, 13);
   if get_Content (a_list) = 13 and then not isEmpty (a_list) then
      Put ("OK");
   else
      Put ("KO");
   end if;
   Put_Line ("");


end test_dbll;
