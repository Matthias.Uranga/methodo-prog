package body file_package is

   --------------------------------------------------------------------- 
   --                   START OF GETTERS & SETTERS                    --
   ---------------------------------------------------------------------
   function get_Name(F_File : in T_File_ptr) return Unbounded_String is       
   begin 
      return F_File.name;
   exception
      when Constraint_Error => return To_Unbounded_String("null"); 
   end get_Name;
   
   procedure set_Name(F_File : in T_File_ptr; new_Name : in Unbounded_String) is      
   begin 
      F_File.name := new_Name;
   end set_Name;
      
   
   function isDir(F_File : in T_File_ptr) return Boolean is      
   begin
      return F_File.isDir;
   end isDir;
   
   procedure set_isDir(F_File : in out T_File_ptr; isItDir : Boolean) is      
   begin
      F_File.isDir := isItDir;
   end set_isDir;
      
   
   function get_Parent(F_File : in T_File_ptr) return T_File_ptr is      
   begin
      return F_File.parent;
   end get_Parent;
      
   procedure set_Parent(F_File : in T_File_ptr; new_Parent : in T_File_ptr) is      
   begin
      F_File.parent := new_Parent;   
   end set_Parent;
   
   function get_Metadata(F_File : in T_File_ptr) return T_Metadata is
   begin
      return F_File.metadata;
   end get_Metadata;
      
   function get_Children(F_File : in T_File_ptr) return FILE_DBLL.T_double_link_list is
      childrenList : FILE_DBLL.T_double_link_list;
   begin
      if not isEmpty(F_File.children) then
         -- if list is not empty
         -- returns the list of children starting on the first element of this list
         childrenList := F_File.children;
         go_To(childrenList, get_First_Content(childrenList));
         return childrenList;         
      end if;       
      -- if the list is empty, simply return the null list
      return F_File.children;
   end get_Children;
   
   function get_Child(F_File : in T_File_ptr; F_childName: Unbounded_String) return T_File_ptr is
      childrenList : FILE_DBLL.T_double_link_list;
   begin
      -- if the list is not empty, look for the child that the name F_childName
      childrenList := get_Children(F_File);
      while get_Name(get_Content(childrenList)) /= F_childName and get_Content(childrenList) /= get_Last_Content(childrenList) loop
         goTo_Next(childrenList);
      end loop;
         
      -- when found, returns the file/directory that childrenList points at (the one that has the name F_childName)
      return get_Content(childrenList);    
   end get_Child;
      
   procedure set_Children(F_File : in T_File_ptr; new_Children : FILE_DBLL.T_double_link_list) is      
   begin
      F_File.children := new_Children;   
   end set_Children;
   
   --------------------------------------------------------------------- 
   --                                                                 --
   ---------------------------------------------------------------------
 
   
   procedure add_Child(File_toAddChildTo : in T_File_ptr; child_toAdd : T_File_ptr) is 
      File_toAddChildTo_Children : FILE_DBLL.T_double_link_list;
   begin
      -- start by getting the children list
      File_toAddChildTo_Children := get_Children(File_toAddChildTo);
      
      if isEmpty(File_toAddChildTo_Children) then  
         -- if children list is empty, add the child as first
         addFirst(File_toAddChildTo_Children, child_toAdd);
      else
         -- if children list is not empty, look for the slot to put the new child
         slotResearch(File_toAddChildTo_Children, child_toAdd);
         if not got_Next(File_toAddChildTo_Children) then
            -- if the returned slot is the last element
            if get_Name(child_toAdd) > get_Name(get_Content(File_toAddChildTo_Children)) then
               -- and if the new child is to be added after the returned slot (last element), add the new child as last
               addLast(File_toAddChildTo_Children, child_toAdd);
            else
               -- if the new child is not to be added after the last element
               if not got_Previous(File_toAddChildTo_Children) then
                  -- and if the returned slot is the first, add the new element as first
                  addFirst(File_toAddChildTo_Children, child_toAdd);
               else
                  -- if the returned slot is not the first element, goes to element just before the returned slot
                  -- and add the new child after it
                  go_To(File_toAddChildTo_Children, get_Previous_Content(File_toAddChildTo_Children));
                  addAfter(File_toAddChildTo_Children, child_toAdd);
               end if;               
            end if;
         else
            -- if none of these conditions are covered, simply add before the returned slot
            addBefore(File_toAddChildTo_Children, child_toAdd);
         end if;         
      end if;
      -- sets the new child's parent to the file you added the child
      set_Parent(child_toAdd, File_toAddChildTo);
      -- resets the file's children list to, make sure changes are affected
      set_Children(File_toAddChildTo, File_toAddChildTo_Children);
   end add_Child;
   
   
   
   procedure slotResearch(children_List : in out FILE_DBLL.T_double_link_list; child_toAdd : in T_File_ptr) is
   begin
      if isEmpty(children_List) then
         -- if the list is empty just gets to any null element of the list, here on the first
         go_To(children_List, get_First_Content(children_List));
      else
         -- if list is not empty move to the next element if it exists and the name of the child to add is alphabetically greater than the current children list
         while got_Next(children_List) loop
            if get_Name(child_toAdd) > get_Name(get_Next_Content(children_List)) then
               goTo_Next(children_List);         
            end if;
         end loop;
      end if;
   exception
         when LIST_EMPTY => raise CHILDREN_LIST_EMPTY;
   end slotResearch;
   
   
   procedure displayChildren(childrenToDisplay : in FILE_DBLL.T_double_link_list) is
      childrenIteration : FILE_DBLL.T_double_link_list := childrenToDisplay;
   begin
      if isEmpty(childrenIteration) then
         -- if list is empty, simply put that it is 
         put("Children list empty");
      else
         -- if list is not empty, make sure to start at the first element of the children list
         go_To(childrenIteration, get_First_Content(childrenIteration));
         loop           
            if get_Extension(get_Metadata(get_Content(childrenIteration))) /= "" then
               -- if current child is a file, put his name and his extension right after
               put(get_Name(get_Content(childrenIteration)));
               put(get_Extension(get_Metadata(get_Content(childrenIteration))));
               exit when not got_Next(childrenIteration);
               -- if current child is the last of the list, exit the loop
               -- else go to next child, and put a comma
               goTo_Next(childrenIteration);
               put(", "); 
            else               
               -- if current child is a directory, only put his name, followed by a comma if its not the last, else exit the loop;
               put(get_Name(get_Content(childrenIteration)));
               exit when not got_Next(childrenIteration);
               goTo_Next(childrenIteration);
               put(", ");    
            end if;            
           end loop;        
      end if;
      put_line("");
   end displayChildren;
      
   --------------------------------------------------------------------- 
   --                  START OF CREATING PROCEDURES                   --
   ---------------------------------------------------------------------  
   procedure createFile(F_File : in out T_File_ptr; new_Parent : T_File_ptr; new_Metadata : T_Metadata; new_Name : Unbounded_String := To_Unbounded_String("New file")) is   
      emptyList : FILE_DBLL.T_double_link_list;
   begin      
      F_File := new T_File;
      set_Name(F_File, new_Name);
      set_isDir(F_File, False);
      set_Parent(F_File, new_Parent);
      F_File.metadata := new_Metadata;
      emptyList := FILE_DBLL.createList;
      set_Children(F_File, emptyList);
   end createFile;
   
   procedure createDir(F_File : in out T_File_ptr; new_Parent : T_File_ptr; new_Metadata : T_Metadata; new_Name : Unbounded_String := To_Unbounded_String("New directory")) is   
      emptyList : FILE_DBLL.T_double_link_list;
   begin 
      F_File := new T_File;
      set_Name(F_File, new_Name);
      set_isDir(F_File, True);
      set_Parent(F_File, new_Parent);
      F_File.metadata := new_Metadata;      
      emptyList := FILE_DBLL.createList;
      set_Children(F_File, emptyList);
   end createDir;
   
   --------------------------------------------------------------------- 
   --                                                                 --
   ---------------------------------------------------------------------
   
   
   function equals(l_file_ptr : T_File_ptr; r_file_ptr : T_File_ptr) return Boolean is
   begin
      return get_Name(l_file_ptr) = get_Name(r_file_ptr);
   end equals;
   
   function greaterThan(l_file_ptr : T_File_ptr; r_file_ptr : T_File_ptr) return Boolean is      
   begin
      return l_file_ptr.name > r_file_ptr.name;
   end greaterThan;
   
   function lowerThan(l_file_ptr : T_File_ptr; r_file_ptr : T_File_ptr) return Boolean is      
   begin
      return l_file_ptr.name < r_file_ptr.name;
   end lowerThan;
   
end file_package;
