package body metadata_package is
   
   ---------------------------------------------------------------
   function get_Author(F_Metadata : in T_Metadata) return Unbounded_String is 
      
   begin
      return F_Metadata.author;
   end get_Author; 
   
   procedure set_Author(F_Metadata : in out T_Metadata; new_Author : in Unbounded_String) is
      
   begin
      F_Metadata.author := new_Author;
   end set_Author; 
   
   ---------------------------------------------------------------
   function get_Rights(F_Metadata : in T_Metadata) return String is
      
   begin
      return F_Metadata.rights;
   end get_Rights;
   
   procedure set_Rights(F_Metadata : in out T_Metadata; new_Rights : in String) is
      
   begin
      F_Metadata.rights := new_Rights;
   end set_Rights;

   ---------------------------------------------------------------
   function get_Size(F_Metadata : in T_Metadata) return integer is 
      
   begin
      return F_Metadata.size;
   end get_Size;
   
   
   procedure set_Size(F_Metadata : in out T_Metadata; new_Size : in integer) is
      
   begin
      F_Metadata.size := new_Size;
   end set_Size;
   
   ---------------------------------------------------------------
   function get_Extension(F_Metadata : in T_Metadata) return Unbounded_String is
      
   begin
      return F_Metadata.extension;
   end get_Extension;
   
   procedure set_Extension(F_Metadata : in out T_Metadata; new_Extension : in Unbounded_String) is
      
   begin
      F_Metadata.extension := new_Extension;
   end set_Extension;
   
   function get_MAX_RIGHTS_STRING_SIZE return Natural is
      
   begin
      return MAX_RIGHTS_STRING_SIZE;
   end get_MAX_RIGHTS_STRING_SIZE;
   
   
   function get_MAX_EXTENSION_STRING_SIZE return Natural is
      
   begin
      return MAX_EXTENSION_STRING_SIZE;
   end get_MAX_EXTENSION_STRING_SIZE;

   
   function createMeta(F_author : Unbounded_String; F_rights : String; F_size : Natural; F_extension : Unbounded_String) return T_Metadata is
      createdMeta : T_Metadata;
   begin
      set_Author(createdMeta, F_author);
      set_Rights(createdMeta, F_rights);
      set_Size(createdMeta, F_size);
      set_Extension(createdMeta, F_extension);
      return createdMeta;
   end createMeta;
      
end metadata_package;
