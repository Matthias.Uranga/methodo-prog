<h1 align="center">USER MANUAL FOR FILE SYSTEM PROJECT</h1>
<hr></br>

<h3><strong>How to launch the file system project</strong></h3>
<p align="justify">
In order to actually use the file system, you have to compile and launch the file <i>sgf_main.adb</i> and when you do so, your terminal will look like this :
</p>
<img src="userManualImages/afterLaunch.png"> </img>
<hr>
</br>
<h3><strong>How to use the file system through its menu</strong></h3>
<p align="justify">
To use the file system with its menu, you actually have to write <b><i>menu</i></b> in the prompt that you have when you launched the file system, as follows :
</p>
<img src="userManualImages/goToMenu.png"> </img>
<p align="justify">
This will modify your terminal to look like this, with available commands and kind of working :
</p>
<img src="userManualImages/menu.png"> </img>
<p align="justify">
If you select 1, as the next figure shows, it will print your working directory :
</p>
<img src="userManualImages/pwdMenu.png"> </img>
<p align="justify">
Selecting 1 and then typing the new directory (or directories) you want to create, will create them for the current file system. Here you can't actually asks to create a directory without typing his absolute path, which.. is a mistake.
</p>
<img src="userManualImages/mkdirMenu.png"> </img>
<p align="justify">
If you select 3, the menu will ask you to type the path (here absolute or relative) to the directory you want to go to, except if you want to go to "/", it will crash :
</p>
<img src="userManualImages/cdMenu.png"> </img>
<p align="justify">
By selecting 4, you will simply exit the menu :
</p>
<img src="userManualImages/menuExit.png"> </img>
</br>
<hr>
</br>

<h3><strong>How to use the file system through the prompt</strong></h3>
<p align="justify">
Typing <i>pwd</i> in the prompt will print your working directory :
</p>
<img src="userManualImages/pwdPrompt.png"> </img>
<p align="justify">
Typing <i>mkdir</i> followed, here too, by only an absolute path, will create, the new directory(ies)
</p>
<img src="userManualImages/mkdirPrompt.png"> </img>
<p align="justify">
If you type <i>cd</i> followed by any path that exists, will change your working directory to it, except for "/"
</p>
<img src="userManualImages/cdPrompt.png"> </img>
<p align="justify">
You are able to exit the prompt, and so the file system, by typing <i>exit</i> in the prompt. 
</p>
<img src="userManualImages/promptExit.png"> </img>