<h1 align="center"><strong>RAPPORT DE PROJET DE METHODOLOGIE DE LA PROGRAMMATION</strong></h1>
</br>
</br>
<div align="justify">
<h2><strong> Objectif du rapport </strong></h2>

<p>
Ce rapport a pour but de présenter comment j'ai travaillé pour résoudre la problématique de création d'un système de gestion de fichier. Il va aussi m'aider à mettre en lumière mes choix algorithmiques, de conception et des types abstraits de données dans le cadre du projet. Ce rapport inclura également une partie bilan, où je ferais état de l'avancement du projet, ou encore des connaissances que j'ai aquises pendant ce mois de travail.
</p>
<hr>
<h2><strong>Introduction</strong></h2>
<p>
Ce projet de méthodologie de la programmation avait pour but la création d'un système de gestion de fichiers simple. Qui dit système de gestion de fichiers dit manipulation, ajout, suppression... de fichiers et/ou répertoires. Avec quelques contraintes de base, le cahier des charges de ce projet n'était justement pas très contraignant, et laissez donc beaucoup de place à notre libre arbitre sur des choix de conception et de réalisation.
Pour répondre à la problématique de la création d'un système de fichier, le présent rapport va suivre le plan suivant : 
</p>
<h3><strong>Sommaire</strong></h3>
<ul>
    <li>Modules & Architecture</li>
    <li>Choix conceptuels</li>
    <li>Démarche des tests</li>
    <li>Bilan</li>
</ul>
<hr>
<h2><strong>Modules & Architecture</strong></h2>
<p>
Cette partie vise à expliciter le fonctionnement des différents modules et de l'architecture générale du projet. </br></br>
Sur la figure suivante on peut voir les différents modules qui rentre en jeu dans le fonctionnement du système de gestion de fichiers. Même si au final, ce dernier ne marcherait pas sans le main_sgf.adb ce n'est pas ce fichier qui met en marche toute la machine derrière le SGF.
</p>
<figure align="center">
    <img src="doc/modules.png"></img>
    <figcaption>Figure représentant les différents modules du système de gestion de fichiers</figcaption>
</figure>
</br> </br>
<p>
Parmi ces modules, celui qui gère la notion d'arborescence pour le SGF est <i>file_package</i>, en effet c'est lui qui gère les enfants d'un fichier ou d'un répertoire, mais aussi son parent, ou encore son nom. Pour une meilleur compréhension j'ai essayé de représenter au mieux ce fonctionnement en arbre au niveau d'un noeud, évidemment la figure suivante peut être répéter le nombre de fois que l'on veut dépendant du nombre de fichier et/ou répertoires que nous voulons dans le SGF.

</p>
<figure align="center">
    <img src="doc/exemple.png"></img>
    <figcaption>Figure représentant la notion d'arbre au niveau d'un de ses noeuds</figcaption>    
</figure>
</br> </br></br>
<hr>
<h2><strong>Choix conceptuels</strong></h2>
<p>
Avoir une architecture stable et robuste a été mon objectif principale pendant ce projet. J'ai pris beaucoup de temps à identifier quels seraient mes modules pour ne pas commencer à coder le système de gestion de fichier en lui-même trop tôt, et donc faire du bricolage en permanence.
</p>
<p>
J'ai donc commencé par comprendre comment travailler autour d'une contrainte majeur, dans le cadrer du projet, imposée par Ada : une utilisation du dynamisme très limitée. De ce que nous avions vu en cours et en tps, très peu de solutions s'offre à quelqu'un qui programme en Ada, hormis en utilisant les listes. Il m'a donc paru logique de travailler avec elles, notamment pour gérer la notion des feuilles de l'arbre comme nous le verrons par la suite. 
</p>
<p>
Ainsi, après avoir discuté longuement avec certains de mes camarades, j'ai pu me conforter à l'idée d'avoir un package me permettant d'avoir une liste doublement chaînée et bornée par un premier et un dernier élément, d'une taille indéfinie. Cette liste, car elle était définie comme générique dans son package m'aura permis notamment de faire une liste doublement chaînée de pointeurs de fichiers pour les enfants d'un répertoire dans le package <i>file_package</i>.
</p>
<p>
Cette conception m'a paru la plus robuste et la plus étendable, car en définissant la liste doublement chainée comme bornée et générique, empêche notamment de gérer un nombre conséquent d'exception liées aux pointeurs null en fin et début de liste. Sa généricité m'aura aussi permis de gérer les arguments d'une commande dans le package <i>sgf_utils_package</i>.
</p>
<p>
D'un autre côté, malgré mon utilisation du package <i>metadata_package</i> permettant aux fichier et/ou répertoire d'avoir des métadonnées, j'ai décidé de gérer, au niveau du noeud de l'arbre, de gérer son nom mais aussi de déterminer si c'est ou non un répertoire. Cette décision a été prise en connaissance de la suite, pour moi, et ça s'est confirmé, il m'a paru plus simple de gérer le nom à ce niveau pour pouvoir agir directement sur les fichiers, sans devoir accéder tout le temps à leurs métadonnées. Ce gain d'efficacité s'est surtout remarqué au niveau du package <i>sgf_utils_package</i> où j'ai gagné en clarté de code. 
</p>

<hr>
<h2><strong>Démarche des tests</strong></h2>
<p>
L'approche de la partie des tests a été pour moi une des moins claires pour ce projet. En effet n'ayant jamais vraiment eu de cours à ce sujet, ni de cas concrets par le passé. Je n'étais pas sûr de comment aborder les tests à première vue. 

J'ai donc fait ce qui m'a paru le plus logique tout en consultant mes camarades à ce sujet : j'ai choisi de faire des tests unitaires et/ou fonctionnels. 
Je me suis dit qu'en testant tous les sous-programmes dans la plupart des cas de figures où ils pourraient être envisagés, leur fonctionnement pouvait être justifié. 

Aussi dans la fin de mon implémentation de la structure, notamment pour le package <i>file_package</i> j'ai travaillé avec une méthode que l'on pourrait rapprocher du Test Driven Development, bien qu'elle m'a beaucoup aidé à identifier les problèmes que mon code avait, j'ai fait évoluer ces tests pour qu'ils ressemblent plus à ce qu'ils sont actuellement, plus formels.
</p>

<hr>
<h2><strong>Bilan</strong></h2>
<h3>Bilan technique</h3>
<p>
Concernant le point de vue technique du projet, je ne considère évidemment pas le projet fini, au vu du pauvre nombre de commandes disponibles pour mon SGF. En revanche, je pense qu'il sera plus ou moins facilement maintenable et extensible par la suite. Pour faire état de sa capacité à être étendu, je n'ai mis qu'une heure environ (par commande), pour faire fonctionner chaque commande disponible aujourd'hui. Ce qui me laisse penser qu'ajouter d'autres commandes devrait pouvoir être fait sans trop de difficultés. </br>
La raison principale de ce manque de commande, est avant liée au temps, en effet j'ai passé beaucoup de temps à concevoir mes modules et mon architecture, ce qui m'a fait défaut par la suite, car je manquais de temps pour leurs implémentations dans un premier temps, puis pour leur utilisation via le SGF. Je tiens à préciser que ce manque de temps n'est pas dû au temps que vous nous laissez, ici encore ce sont des choix personnels, je n'ai pas voulu m'investir à 100% dans le projet, afin de ne pas délaisser les autres cours.
</p>

<h3>Bilan personnel</h3>
<p>
Etant perfectionniste, l'état d'avancement de mon projet me déçoit pas mal, mais comme dit plus haut, il est principalement dû aux choix que j'ai fait, et si je l'avais voulu j'aurai pu en être plus loin à ce jour. Au début j'avoue ne pas avoir totalement accroché au sujet, surtout en voyant la nécessité du dynamisme, concept assez limité en Ada, mais au final, plus j'avançais dans ma conception plus je prenais goût à le réaliser. Si je devais faire, un découpage de mon temps passé aux diverses étapes du projet, il serait le suivant : 
<ul>
    <li>45% de conception</li>
    <li>30% d'implémentation</li>
    <li>15% pour les tests</li>
    <li>10% pour le rapport</li>
</ul>
Si je dois tirer un enseignement de ce rapport, c'est bien celui de trouver de la motivation dans ce qui ne me plait pas forcément au premier abord. Car si je m'étais motivé à commencer tout plus tôt, j'aurai pu en être beaucoup plus loin aujourd'hui. 
</p>
</div>